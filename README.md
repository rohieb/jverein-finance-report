Finanzreport für JVerein
========================

Generiert einen Finanzreport mit übersichtlichen Tabellen. Beispiel unter
<http://stuff.tinyhost.de/s0finanz/data/report.html>


Projekt bauen
-------------

Importier das Project in Eclipse. Es ist einfacher, als sich mit
Java-Build-Tools rumzuschlagen.

Der Sourcecode liegt in `src`, `lib/h2-*.jar` muss in den Classpath aufgenommen
werden.


Benutzung
---------

Die `org.Stratum0.vorstand.guv.Main.main()` erwartet drei Parameter:

1. Pfad zur JVerein-H2DB
2. Ausgabepfad
3. Pfad zur `verträge.csv`

Als Bequemlichkeit existiert `make-report.sh`, das als einzigen Parameter den
Pfad zum Finanzen-Git-Repo erwartet und die Ausgabe in `report/` ablegt.
