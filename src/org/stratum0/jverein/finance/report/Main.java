package org.stratum0.jverein.finance.report;

import java.util.List;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.StringTokenizer;

public class Main {

    static String outDir;

    static String header = "<!DOCTYPE html>\n<html lang=\"de\">\n<head>\n  <meta charset=\"utf-8\">\n    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n    <title>Stratum 0 e.V. - Finanzübersicht</title>\n\n    <!-- Bootstrap -->\n    <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">\n   <style>\ntd.number, th.number, table#months tr td {\n  text-align: right;\n}\n\ntable#months tr td:first-child {\n text-align: left;\n}\n\ntfoot {\n    font-weight: bold;\n}</style>\n  </head>\n<body>\n  <div class=\"container\">\n<h1>Stratum 0 e.V. - Finanz&uuml;bersicht</h1>";
    static String footer = "<script src=\"jquery-1.11.1.min.js\"></script>\n<script src=\"js/bootstrap.min.js\"></script>\n<script>\n$(function(){\n	//alert(1);\n	$(\"table > tbody > tr > td\").each(function() {\n		var child = $(this);\n		var floatval = parseFloat(child.html());\n		if(floatval < 0) {\n			child.css({\"color\":\"red\"});\n		}\n		if(child.html() == floatval.toFixed(1)) {\n			child.html(floatval.toFixed(2));\n		}\n	});\n});\n</script>\n</html>";

    public static void main(String[] args) throws Exception {

        if (args.length < 3) {
            throw new Exception(
                    "Nicht genügend Parameter.\r\n\r\nGib mir Pfade zu:\r\n  1) .jameica/jverein/h2db/jverein.h2.db\r\n  2) Ausgabeverzeichnis\r\n  3) verträge.csv");
        }

        String jdbcFile = args[0];
        outDir = args[1];
        String contractFile = args[2];

        // h2db hängt die Dateiendung automatisch nochmal dran...
        if (jdbcFile.endsWith(".h2.db")) {
            jdbcFile = jdbcFile.substring(0, jdbcFile.length() - 6);
        }

        Class.forName("org.h2.Driver");
        Connection conn = DriverManager.getConnection("jdbc:h2:file:"
                + jdbcFile, "jverein", "jverein");
        System.out.println("Verbindung hergestellt");

        PrintWriter html = new PrintWriter(outDir + "report.html", "UTF-8");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        java.util.Date d = new java.util.Date();

        html.print(header);
        html.println("Erzeugt am: " + df.format(d));

        html.println("<h2>"
                + HTMLEntities.htmlentities("Aktueller Kontostand:") + "</h2>");
        html.println("Diese Daten als <a href=\"guthaben.csv\" target=\"_blank\">CSV</a>.");
        getSum(conn, html);

        html.println("<h2>"
                + HTMLEntities
                        .htmlentities("Aktuelle monatliche Verpflichtungen:")
                + "</h2>");
        getContracts(html, contractFile);

        html.println("</div>\n<div class=\"container\">");
        html.println("<h2>"
                + HTMLEntities.htmlentities("Aktuelle Zweckbindungen:")
                + "</h2>");
        getWeicheKonten(conn, html);

        html.println("</div>\n<div class=\"container\">");
        html.println("<h2>"
                + HTMLEntities.htmlentities("Einnahmen / Ausgaben über Zeit:")
                + "</h2>");
        html.println("Diese Daten als <a href=\"guv.csv\" target=\"_blank\">CSV</a>.");
        getArtSum(conn, html);

        html.println("</div>");

        html.println(footer);

        html.close();
        System.err.println("- Finanzstatistik erzeugt.");

        // Mitgliederstatistik
        PrintWriter mitgliedsCsv = new PrintWriter(outDir + "mitglieder.csv",
                "UTF-8");
        mitgliederStat(conn, mitgliedsCsv);
        mitgliedsCsv.close();
        System.err.println("- Mitgliederstatistik erzeugt.");

        // sanityCheckBuchungen(conn);

        conn.close();

        System.out.println("Abgeschlossen");

    }

    private static double roundToCent(double in) {
        return Math.round(in * 100) / 100.0;
    }

    private static void getContracts(PrintWriter html, String contractFile)
            throws Exception {
        BufferedReader br = new BufferedReader(new FileReader(contractFile));
        String strLine = "";
        StringTokenizer st = null;
        int tokenNumber = 0;
        boolean openRow;
        double totalAmount = 0;

        html.println("<table id='contracts' class='table table-striped'>");
        html.println("<thead><tr><th>Kategorie</th><th>Bezeichnung</th><th class='number'>Betrag [EUR]</th></tr></thead>");
        html.println("<tbody>");

        // read character separated file line by line
        while ((strLine = br.readLine()) != null) {
            // ignore comment lines
            if (strLine.startsWith("#")) {
                continue;
            }

            tokenNumber = 0;
            openRow = false;

            // break character separated line using ";"
            st = new StringTokenizer(strLine, ";");

            while (st.hasMoreTokens()) {
                String field = st.nextToken();

                if (!openRow) {
                    html.print("<tr>");
                    openRow = true;
                }

                System.out.println(field);
                if (tokenNumber == 2) {
                    double val = NumberFormat.getInstance(Locale.US)
                            .parse(field).doubleValue();
                    totalAmount += val;
                    html.printf("<td class='number'>%.02f</td>", val);
                } else {
                    // note: no escaping here, may contain HTML
                    html.print("<td>" + field + "</td>");
                }

                tokenNumber++;
            }
        }

        html.println("</tbody>");
        html.printf(
                "<tfoot><tr><td colspan='2'>Summe</td><td class='number'>%.02f</td></tr></th>",
                totalAmount);
        html.println("</tfoot>");
        html.println("</table>");

        br.close();
    }

    private static void getGuV(Connection conn, PrintWriter html)
            throws Exception {
        // Prepare Calendar for Iteration
        Calendar iterFom = Calendar.getInstance();
        iterFom.set(Calendar.HOUR, 0);
        iterFom.set(Calendar.MINUTE, 0);
        iterFom.set(Calendar.SECOND, 0);
        iterFom.set(Calendar.MILLISECOND, 0);
        iterFom.set(Calendar.DAY_OF_MONTH,
                iterFom.getActualMinimum(Calendar.DAY_OF_MONTH));

        Calendar iterLom = (Calendar) iterFom.clone();
        iterLom.set(Calendar.DAY_OF_MONTH,
                iterLom.getActualMaximum(Calendar.DAY_OF_MONTH));

        Calendar stop = Calendar.getInstance();
        stop.set(Calendar.YEAR, 2011);
        stop.set(Calendar.MONTH, Calendar.MAY);
        stop.set(Calendar.DAY_OF_MONTH, 2);

        while (stop.before(iterFom)) {
            System.out.println("Start: " + iterFom.get(Calendar.YEAR) + "-"
                    + iterFom.get(Calendar.MONTH) + "-"
                    + iterFom.get(Calendar.DAY_OF_MONTH));
            System.out.println("END:   " + iterLom.get(Calendar.YEAR) + "-"
                    + iterLom.get(Calendar.MONTH) + "-"
                    + iterLom.get(Calendar.DAY_OF_MONTH));

            /*
             * PreparedStatement st = conn.prepareStatement(
             * "SELECT DATUM, BETRAG FROM BUCHUNG AS B WHERE B.DATUM >= ? and B.DATUM <= ? ORDER BY B.DATUM"
             * ); st.setDate(1, new java.sql.Date(iterFom.getTimeInMillis()));
             * st.setDate(2, new java.sql.Date(iterLom.getTimeInMillis()));
             * ResultSet str= st.executeQuery();
             * 
             * double sum = 0; while(str.next()) { java.sql.Date d =
             * str.getDate(1); Calendar dc = Calendar.getInstance();
             * dc.setTime(d); System.out.println("Buchung "+
             * dc.get(Calendar.YEAR)+ "-" + (dc.get(Calendar.MONTH)+1)+ "-" +
             * dc.get(Calendar.DAY_OF_MONTH)+": "+str.getDouble(2)); sum
             * +=str.getDouble(2); } System.out.println("Summe: "+ sum);
             */

            html.print("<tr>");
            html.print("<td>"
                    + HTMLEntities.htmlentities("" + iterFom.get(Calendar.YEAR)
                            + "-" + (iterFom.get(Calendar.MONTH) + 1))
                    + "</td>");

            PreparedStatement monatsSummePs = conn
                    .prepareStatement("SELECT SUM(BU.BETRAG) FROM BUCHUNG AS BU WHERE DATUM >= ? and DATUM <= ?;");
            monatsSummePs.setDate(1,
                    new java.sql.Date(iterFom.getTimeInMillis()));
            monatsSummePs.setDate(2,
                    new java.sql.Date(iterLom.getTimeInMillis()));
            ResultSet monatsSummenRes = monatsSummePs.executeQuery();

            while (monatsSummenRes.next()) {
                html.print("<td>"
                        + HTMLEntities.htmlentities(""
                                + roundToCent(monatsSummenRes.getDouble(1)))
                        + "</td>");
                // System.out.println("Summe aus DB: "+monatsSummenRes.getDouble(1));
            }

            PreparedStatement klassenSummenPs = conn
                    .prepareStatement("SELECT SUM(BU.BETRAG),BK.BEZEICHNUNG,	BK.NUMMER FROM BUCHUNGSKLASSE AS BK LEFT JOIN BUCHUNGSART AS BA ON BK.ID = BA.BUCHUNGSKLASSE LEFT JOIN BUCHUNG AS BU ON BU.BUCHUNGSART = BA.ID AND BU.DATUM >= ? AND BU.DATUM <= ? GROUP BY BK.NUMMER ORDER BY BK.NUMMER");
            klassenSummenPs.setDate(1,
                    new java.sql.Date(iterFom.getTimeInMillis()));
            klassenSummenPs.setDate(2,
                    new java.sql.Date(iterLom.getTimeInMillis()));
            ResultSet klassenSummenRes = klassenSummenPs.executeQuery();

            while (klassenSummenRes.next()) {
                html.print("<td>"
                        + HTMLEntities.htmlentities(""
                                + roundToCent(klassenSummenRes.getDouble(1)))
                        + "</td>");
                // System.out.println("Nummer: "+klassenSummenRes.getString(3)+" || Summe: "+klassenSummenRes.getDouble(1));
            }
            /*
             * TODO: This is broken due to Jverein-Messing with Primary Keys.
             * Also it makes the output looking so ugly ^^. PreparedStatement
             * detailPs = conn.prepareStatement(
             * "SELECT SUM(BU.BETRAG), BK.BEZEICHNUNG FROM BUCHUNGSART AS BA INNER JOIN BUCHUNGSKLASSE AS BK 	LEFT JOIN BUCHUNG AS BU	ON BU.BUCHUNGSART = BA.ID AND BU.DATUM >= ? AND BU.DATUM <= ? ON BA.BUCHUNGSKLASSE = BK.ID  GROUP BY BA.ID ORDER BY BK.ID, BA.ID;"
             * ); detailPs.setDate(1, new
             * java.sql.Date(iterFom.getTimeInMillis())); detailPs.setDate(2,
             * new java.sql.Date(iterLom.getTimeInMillis())); ResultSet
             * detailRes = detailPs.executeQuery();
             * 
             * while (detailRes.next()) { html.print( "<td>" +
             * HTMLEntities.htmlentities(""+roundToCent(detailRes.getDouble(1)))
             * + "</td>"); }
             */

            html.println("</tr>");

            iterFom.add(Calendar.MONTH, -1);
            iterLom.add(Calendar.MONTH, -1);
            iterFom.set(Calendar.DAY_OF_MONTH,
                    iterFom.getActualMinimum(Calendar.DAY_OF_MONTH));
            iterLom.set(Calendar.DAY_OF_MONTH,
                    iterLom.getActualMaximum(Calendar.DAY_OF_MONTH));
        }
    }

    private static void getWeicheKonten(Connection conn, PrintWriter html)
            throws Exception {
        float totalSum = 0;

        html.println("<table id=\"weicheKonten\" class=\"table table-striped\">");
        html.println("<thread>");
        html.println("<tr><th>Zweckbindung für</th><th>Zusammengefasste Buchungarten</th><th class='number'>Summe [EUR]</th></tr>");
        html.println("</thread>\n<tbody>");

        List<Zweckbindung> zweckbindungen = new LinkedList<Zweckbindung>();
        zweckbindungen.add(new Zweckbindung("Freifunk",
                new int[] { 1108, 1109 }));
        zweckbindungen.add(new Zweckbindung("Coder Dojo", new int[] { 1110,
                1111 }));
        // zweckbindungen.add(new Zweckbindung("Material 3D-Druck", new
        // int[]{1106,1107}));
        // zweckbindungen.add(new Zweckbindung("Material Stickmaschine", new
        // int[]{1102,1103}));
        // zweckbindungen.add(new Zweckbindung("Material Plotter", new
        // int[]{1104,1105}));
        // zweckbindungen.add(new Zweckbindung("Bastelmaterial allgemein", new
        // int[]{1100,1101}));

        for (Zweckbindung z : zweckbindungen) {
            totalSum += roundToCent(z.getSum(conn));
            html.printf(
                    "<tr><td>%s</td><td>%s</td><td class='number'>%.02f</td></tr>\n",
                    HTMLEntities.htmlentities(z.getName()),
                    HTMLEntities.htmlentities(z.getBuchungsarten(conn, true)),
                    roundToCent(z.getSum(conn)));
        }

        html.printf(
                "</tbody>\n<tfoot><tr><td colspan='2'>Summe</td><td class='number'>%.02f"
                        + "</td></tr>\n</tfoot></table>\n", totalSum);
    }

    private static void getSum(Connection conn, PrintWriter html)
            throws Exception {

        PrintWriter pw = new PrintWriter(outDir + "guthaben.csv", "UTF-8");

        PreparedStatement st = conn
                .prepareStatement("SELECT K.NUMMER AS \"Interne Kontonummer\", K.BEZEICHNUNG AS \"Bezeichnung\", SUM(B.BETRAG) AS \"Guthaben\" FROM BUCHUNG AS B, KONTO AS K WHERE B.KONTO = K.ID GROUP BY K.ID ORDER BY K.NUMMER;");
        ResultSet res = st.executeQuery();

        pw.println("Interne Kontonummer; Bezeichnung; Guthaben");

        html.println("<table id=\"overview\" class=\"table table-striped\">");
        html.println("<thread>");
        html.println("<tr><th>Interne Kontonummer</th><th>Bezeichnung</th><th class='number'>Guthaben [EUR]</th></tr>");
        html.println("</thread>\n<tbody>");

        double totalSum = 0;
        while (res.next()) {
            double value = Math.round(res.getDouble(3) * 100) / 100.0;
            pw.printf("%s;%s;%.02f\n", res.getString(1), res.getString(2),
                    value);
            html.printf(
                    "<tr><td>%s</td><td>%s</td><td class='number'>%.02f</td></tr>\n",
                    HTMLEntities.htmlentities(res.getString(1)),
                    HTMLEntities.htmlentities(res.getString(2)), value);
            totalSum += value;
        }

        html.printf(
                "</tbody>\n<tfoot><tr><td colspan='2'>Summe</td><td class='number'>%.02f"
                        + "</td></tr>\n</tfoot></table>\n", totalSum);

        pw.close();
    }

    private static void getArtSum(Connection conn, PrintWriter html)
            throws Exception {
        Calendar iterFom = Calendar.getInstance();
        iterFom.set(Calendar.HOUR, 0);
        iterFom.set(Calendar.MINUTE, 0);
        iterFom.set(Calendar.SECOND, 0);
        iterFom.set(Calendar.MILLISECOND, 0);
        iterFom.set(Calendar.DAY_OF_MONTH,
                iterFom.getActualMinimum(Calendar.DAY_OF_MONTH));

        Calendar iterLom = (Calendar) iterFom.clone();
        iterLom.set(Calendar.DAY_OF_MONTH,
                iterLom.getActualMaximum(Calendar.DAY_OF_MONTH));

        Calendar stop = Calendar.getInstance();
        stop.set(Calendar.YEAR, 2011);
        stop.set(Calendar.MONTH, Calendar.MAY);
        stop.set(Calendar.DAY_OF_MONTH, 2);

        html.println("<table id=\"months\" class=\"table table-striped\">\n<thread>");
        html.print("<tr>");
        html.print("<th>Monat</th><th>Summe gesamter Monat:</th>");

        PrintWriter csv = new PrintWriter(outDir + "guv.csv", "UTF-8");
        csv.println("Stratum 0 e.V. Gewinn- und Verlustrechnung");
        csv.println("Version, 1");
        csv.println("Timestamp, " + (int) (System.currentTimeMillis() / 1000L));
        csv.print("Monat,Summe gesamter Monat");

        PreparedStatement klassenSummenPh = conn
                .prepareStatement("SELECT BK.BEZEICHNUNG FROM BUCHUNGSKLASSE AS BK ORDER BY BK.NUMMER;");
        ResultSet klassenSummenResh = klassenSummenPh.executeQuery();
        while (klassenSummenResh.next()) {
            html.print("<th>"
                    + HTMLEntities.htmlentities(klassenSummenResh.getString(1))
                    + "</th>");
            csv.print("," + klassenSummenResh.getString(1));
        }

        PreparedStatement ph = conn
                .prepareStatement("SELECT BA.BEZEICHNUNG, BK.BEZEICHNUNG FROM BUCHUNGSART AS BA INNER JOIN BUCHUNGSKLASSE AS BK 	ON BA.BUCHUNGSKLASSE = BK.ID ORDER BY BK.NUMMER, BA.NUMMER;");
        ResultSet resh = ph.executeQuery();
        while (resh.next()) {
            html.print("<th>"
                    + HTMLEntities.htmlentities(resh.getString(2) + ": "
                            + resh.getString(1)) + "</th>");
            csv.print("," + resh.getString(2) + ": " + resh.getString(1));
        }

        html.println("</tr>\n</thread>\n<tbody>");
        csv.println("");

        while (stop.before(iterFom)) {
            System.out.println("Start: " + iterFom.get(Calendar.YEAR) + "-"
                    + iterFom.get(Calendar.MONTH) + "-"
                    + iterFom.get(Calendar.DAY_OF_MONTH));
            System.out.println("END:   " + iterLom.get(Calendar.YEAR) + "-"
                    + iterLom.get(Calendar.MONTH) + "-"
                    + iterLom.get(Calendar.DAY_OF_MONTH));

            /*
             * PreparedStatement st = conn.prepareStatement(
             * "SELECT DATUM, BETRAG FROM BUCHUNG AS B WHERE B.DATUM >= ? and B.DATUM <= ? ORDER BY B.DATUM"
             * ); st.setDate(1, new java.sql.Date(iterFom.getTimeInMillis()));
             * st.setDate(2, new java.sql.Date(iterLom.getTimeInMillis()));
             * ResultSet str= st.executeQuery();
             * 
             * double sum = 0; while(str.next()) { java.sql.Date d =
             * str.getDate(1); Calendar dc = Calendar.getInstance();
             * dc.setTime(d); System.out.println("Buchung "+
             * dc.get(Calendar.YEAR)+ "-" + (dc.get(Calendar.MONTH)+1)+ "-" +
             * dc.get(Calendar.DAY_OF_MONTH)+": "+str.getDouble(2)); sum
             * +=str.getDouble(2); } System.out.println("Summe: "+ sum);
             */

            html.print("<tr>");
            html.print("<td>"
                    + HTMLEntities.htmlentities("" + iterFom.get(Calendar.YEAR)
                            + "-" + (iterFom.get(Calendar.MONTH) + 1))
                    + "</td>");
            csv.print(iterFom.get(Calendar.YEAR) + "-"
                    + (iterFom.get(Calendar.MONTH) + 1));

            PreparedStatement monatsSummePs = conn
                    .prepareStatement("SELECT SUM(BU.BETRAG) FROM BUCHUNG AS BU WHERE DATUM >= ? and DATUM <= ?;");
            monatsSummePs.setDate(1,
                    new java.sql.Date(iterFom.getTimeInMillis()));
            monatsSummePs.setDate(2,
                    new java.sql.Date(iterLom.getTimeInMillis()));
            ResultSet monatsSummenRes = monatsSummePs.executeQuery();

            while (monatsSummenRes.next()) {
                html.print("<td>"
                        + HTMLEntities.htmlentities(""
                                + roundToCent(monatsSummenRes.getDouble(1)))
                        + "</td>");
                // System.out.println("Summe aus DB: "+monatsSummenRes.getDouble(1));
                csv.print("," + roundToCent(monatsSummenRes.getDouble(1)));
            }

            PreparedStatement klassenSummenPs = conn
                    .prepareStatement("SELECT SUM(BU.BETRAG),BK.BEZEICHNUNG,	BK.NUMMER FROM BUCHUNGSKLASSE AS BK LEFT JOIN BUCHUNGSART AS BA ON BK.ID = BA.BUCHUNGSKLASSE LEFT JOIN BUCHUNG AS BU ON BU.BUCHUNGSART = BA.ID AND BU.DATUM >= ? AND BU.DATUM <= ? GROUP BY BK.NUMMER ORDER BY BK.NUMMER");
            klassenSummenPs.setDate(1,
                    new java.sql.Date(iterFom.getTimeInMillis()));
            klassenSummenPs.setDate(2,
                    new java.sql.Date(iterLom.getTimeInMillis()));
            ResultSet klassenSummenRes = klassenSummenPs.executeQuery();

            while (klassenSummenRes.next()) {
                html.print("<td>"
                        + HTMLEntities.htmlentities(""
                                + roundToCent(klassenSummenRes.getDouble(1)))
                        + "</td>");
                // System.out.println("Nummer: "+klassenSummenRes.getString(3)+" || Summe: "+klassenSummenRes.getDouble(1));
                csv.print("," + roundToCent(klassenSummenRes.getDouble(1)));
            }
            PreparedStatement detailPs = conn
                    .prepareStatement("SELECT SUM(BU.BETRAG), BK.BEZEICHNUNG FROM BUCHUNGSART AS BA INNER JOIN BUCHUNGSKLASSE AS BK 	LEFT JOIN BUCHUNG AS BU	ON BU.BUCHUNGSART = BA.ID AND BU.DATUM >= ? AND BU.DATUM <= ? ON BA.BUCHUNGSKLASSE = BK.ID  GROUP BY BA.ID ORDER BY BK.NUMMER, BA.NUMMER;");
            detailPs.setDate(1, new java.sql.Date(iterFom.getTimeInMillis()));
            detailPs.setDate(2, new java.sql.Date(iterLom.getTimeInMillis()));
            ResultSet detailRes = detailPs.executeQuery();

            while (detailRes.next()) {
                html.print("<td>"
                        + HTMLEntities.htmlentities(""
                                + roundToCent(detailRes.getDouble(1)))
                        + "</td>");
                csv.print("," + roundToCent(detailRes.getDouble(1)));
            }

            html.println("</tr>");
            csv.println();

            iterFom.add(Calendar.MONTH, -1);
            iterLom.add(Calendar.MONTH, -1);
            iterFom.set(Calendar.DAY_OF_MONTH,
                    iterFom.getActualMinimum(Calendar.DAY_OF_MONTH));
            iterLom.set(Calendar.DAY_OF_MONTH,
                    iterLom.getActualMaximum(Calendar.DAY_OF_MONTH));
        }

        html.println("</tbody></table>");
        csv.close();
    }

    private static void mitgliederStat(Connection conn, PrintWriter csv)
            throws Exception {
        Calendar iterFom = Calendar.getInstance();
        iterFom.set(Calendar.HOUR, 0);
        iterFom.set(Calendar.MINUTE, 0);
        iterFom.set(Calendar.SECOND, 0);
        iterFom.set(Calendar.MILLISECOND, 0);
        iterFom.set(Calendar.YEAR, 2011);
        iterFom.set(Calendar.MONTH, Calendar.MAY);
        iterFom.set(Calendar.DAY_OF_MONTH,
                iterFom.getActualMinimum(Calendar.DAY_OF_MONTH));

        Calendar stop = Calendar.getInstance();

        csv.println(HTMLEntities
                .htmlentities("Monat,Mitglieder Gesamt,Normalzahler,Ermäßigt,Fördermitglieder"));
        csv.println("#Stratum 0 e.V. Mitgliederentwicklung");
        csv.println("#Version, 1");
        csv.println("#Timestamp, " + (int) (System.currentTimeMillis() / 1000L));

        while (iterFom.before(stop)) {
            PreparedStatement mitgliederPS = conn
                    .prepareStatement("SELECT COUNT(M.ID) FROM Mitglied AS M WHERE M.Eintritt <= ? AND (M.Austritt > ? OR M.Austritt IS NULL);");
            mitgliederPS.setDate(1,
                    new java.sql.Date(iterFom.getTimeInMillis()));
            mitgliederPS.setDate(2,
                    new java.sql.Date(iterFom.getTimeInMillis()));
            ResultSet mitgliederRS = mitgliederPS.executeQuery();

            PreparedStatement mitgliederGruppenPS = conn
                    .prepareStatement("SELECT COUNT( M.ID ), B.BEZEICHNUNG FROM MITGLIED AS M, BEITRAGSGRUPPE AS B WHERE M.Eintritt <= ? AND (M.Austritt > ? OR M.Austritt IS NULL)  AND B.ID = M.BEITRAGSGRUPPE AND (B.ID = 1 OR B.ID = 2 OR B.ID=6) AND M.INDIVIDUELLERBEITRAG = 0 GROUP BY B.ID;");
            mitgliederGruppenPS.setDate(1,
                    new java.sql.Date(iterFom.getTimeInMillis()));
            mitgliederGruppenPS.setDate(2,
                    new java.sql.Date(iterFom.getTimeInMillis()));
            ResultSet mitgliederGruppenRS = mitgliederGruppenPS.executeQuery();

            csv.print("" + iterFom.get(Calendar.YEAR) + "-"
                    + (iterFom.get(Calendar.MONTH) + 1) + "-"
                    + iterFom.get(Calendar.DAY_OF_MONTH) + ",");
            if (mitgliederRS.next()) {
                csv.print(mitgliederRS.getInt(1) + ",");
            } else {
                csv.print("0,");
            }

            for (int i = 0; i < 3; i++) {
                if (mitgliederGruppenRS.next()) {
                    csv.print(mitgliederGruppenRS.getInt(1));
                } else {
                    csv.print("0");
                }
                if (i < 2) {
                    csv.print(",");
                }

            }
            csv.println();

            iterFom.add(Calendar.MONTH, 1);
            iterFom.set(Calendar.DAY_OF_MONTH,
                    iterFom.getActualMinimum(Calendar.DAY_OF_MONTH));

        }
    }

    private static void sanityCheckBuchungen(Connection conn) throws Exception {
        // Erzeuge Liste aller Buchungen in der Datenbank
        PreparedStatement ps = conn
                .prepareStatement("SELECT ID, KONTO, NAME, BETRAG, ZWECK, DATUM FROM BUCHUNG");
        ResultSet rs = ps.executeQuery();

        List<Buchung> buchung = new LinkedList<Buchung>();

        while (rs.next()) {
            buchung.add(new Buchung(rs));
        }

        System.err
                .println("Suche in "
                        + buchung.size()
                        + " Buchungen nach doppelten Buchungen. Was für eine doofe Idee Oo");

        // Prüfe jede Buchung gegen jede, ob sie Doppelt sein könnte.
        Buchung ba;
        Buchung bb;
        for (int i = 0; i < (buchung.size() - 1); i++) {
            ba = buchung.get(i);
            for (int j = i + 1; j < buchung.size(); j++) {
                bb = buchung.get(j);
                if (ba.KONTO == bb.KONTO && ba.BETRAG == bb.BETRAG
                        && ba.ZWECK.equalsIgnoreCase(bb.ZWECK)
                        && ba.NAME.equalsIgnoreCase(bb.NAME)
                        && ba.DATUM.compareTo(bb.DATUM) == 0) {
                    System.err.println("Auf Konto " + ba.KONTO + ": Umsatz-ID "
                            + ba.ID + " ähnelt " + bb.ID);
                }
            }
        }

    }

}
