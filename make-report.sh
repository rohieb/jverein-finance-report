#!/bin/sh

if [ -z "$1" ]; then
	echo "Bitte gib mir den Pfad vom Finanzen-Git-Repository."
	exit 1
fi

if [ ! -f bin/org/stratum0/jverein/finance/report/Main.class ]; then
	echo "Hey, du musst das Projekt erst bauen."
	exit 1
fi

mkdir -p report/
java -classpath bin:lib/h2-1.3.175.jar org.stratum0.jverein.finance.report.Main \
	"$1/.jameica/jverein/h2db/jverein.h2.db" report/ "$1/verträge.csv"

echo
echo "Okay. Alles liegt jetzt unter report/."
