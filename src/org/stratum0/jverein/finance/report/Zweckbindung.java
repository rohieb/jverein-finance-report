package org.stratum0.jverein.finance.report;

import java.util.LinkedList;
import java.util.List;
import java.sql.*;

public class Zweckbindung {
    String name;
    int[] buchungsartenID;
    List<String> buchungsartenNamen = new LinkedList<String>();

    public Zweckbindung(String name, int[] buchungsarten) {
        this.name = name;
        this.buchungsartenID = buchungsarten;
    }

    public String getName() {
        return name;
    }

    public String getBuchungsarten(Connection conn, Boolean htmlStyle)
            throws Exception {
        for (int num : buchungsartenID) {
            PreparedStatement st = conn
                    .prepareStatement("SELECT B.BEZEICHNUNG FROM BUCHUNGSART AS B WHERE B.NUMMER = ? LIMIT 1;");
            st.setInt(1, num);
            ResultSet res = st.executeQuery();
            if (!res.next()) {
                throw new Exception("Buchungsart mit ID " + num
                        + " nicht gefunden.");
            }
            buchungsartenNamen.add(res.getString(1));
        }

        String s = "";
        for (String sub : buchungsartenNamen) {
            if (htmlStyle) {
                s += sub + "<br />";
            } else {
                s += sub + ",";
            }
        }
        return s;
    }

    public double getSum(Connection conn) throws Exception {
        double sum = 0;
        for (int num : buchungsartenID) {
            PreparedStatement st = conn
                    .prepareStatement("SELECT SUM(BU.BETRAG) FROM BUCHUNG AS BU, BUCHUNGSART AS BA WHERE BU.BUCHUNGSART = BA.ID AND BA.NUMMER = ?;");
            st.setInt(1, num);
            ResultSet res = st.executeQuery();
            if (!res.next()) {
                throw new Exception("Buchungsart mit ID " + num
                        + " nicht gefunden.");
            }
            sum += res.getDouble(1);
        }
        return sum;
    }

}
