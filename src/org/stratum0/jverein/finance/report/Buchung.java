package org.stratum0.jverein.finance.report;

import java.sql.ResultSet;
import java.util.Date;

public class Buchung {
    public int ID;
    public int KONTO;
    public String NAME;
    public double BETRAG;
    public String ZWECK;
    public Date DATUM;

    public Buchung(ResultSet rs) throws Exception {
        ID = rs.getInt(1);
        KONTO = rs.getInt(2);
        NAME = rs.getString(3);
        BETRAG = rs.getDouble(4);
        ZWECK = rs.getString(5);
        DATUM = rs.getDate(6);
    }
}
